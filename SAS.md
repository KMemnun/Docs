
proc sql;
connect to oracle as myconn (USER='' PASSWORD=''
PATH='(DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST AA.XX.com.tr)(PORT = 1234))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = MYDB)
    ))');

create table work.test as
select * from connection to myconn


(select *
 from testdb.test_data
 
);

disconnect from myconn;

quit;


LIBNAME MYLIB ORACLE USERNAME='X' PASSWORD='x' PATH = '(DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = AA.XX.com.tr)(PORT = 1234))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = MYDB)
    ))';
data WORK.test2; /* DB'deki HEDEF TABLO ŞEMASI VE ADI */
  set WORK.test1; /* SAS TABLOSU */
run;



SAS PASSWORD ENCRYPTION 

proc pwencode in='mypassword' method=sasenc; run;


LOG BASTIRMA
outallprice = miprslt.allprice(where=(ReturnedValue=1))
tracelog=PUT;


proc sql;
create table test as select * from dictionary.tables where libname='MYLIB';
;run;

options dlcreatedir ;
libname sas_econ "/olusturulacak_yol";
options nodlcreatedir ;



YEAR(DATEPART(AS_OF_DATE))

INTCK('DTMONTH',DEFAULTDATE,BALANCESHEETDATE)

PUT(B.TKP_BAS_TAR, YYMMD7.) AS COHORT /* DATETIME İSE DATEPART İLE DATE'E ÇEVİR */

PUT(B.TKP_BAS_TAR, yymmn6.) AS COHORT /* DATETIME İSE DATEPART İLE DATE'E ÇEVİR */


-----------------------------------------

ods trace on;
ods output EngineHost=WithOwner;
proc datasets lib=MYLIB;
 contents data=TABLO_DATA;
quit;

data WithOwner;
 set WithOwner (where=(label1 in ('Filename' 'Owner Name')));
run;


PROC MEANS DATA= DATALIB.TABLO_DATA(where=(SIDE IN ('12')))
	MEAN STD MIN MAX RANGE N NMISS P1 P5 P10 Q1 MEDIAN Q3 P90 P95 P99 STACKODSOUTPUT;
	VAR _NUMERIC_;
RUN;





proc tabulate data=sashelp.class out=want;
  class Sex Age;
  table sex all,(Age='Age (%)' all)*PCTN;
run;


NUMERIK ANALİZLER
/*PROC SQL;*/
/*CREATE VIEW work.SORTTempTableSorted AS	*/
/*	SELECT * FROM work.data_table AS T;*/
/*	QUIT;*/



ODS OUTPUT Summary= Numeric;

PROC MEANS DATA= work.data_table
	MEAN STD MIN MAX RANGE N NMISS P1 P5 P10 Q1 MEDIAN Q3 P90 P95 P99 STACKODSOUTPUT;
	VAR _Numeric_;
	RUN;



proc format;
  value zerof
  0='Zero'
  .='Missing'
  other='Not Zero';
quit;


proc freq data=work.data_table;
  format _numeric_ zerof.;
  table _numeric_/missing;
run;






EXCEL OLUŞTURMA MACROSU
%macro create_xls();
%let now=%sysfunc(datetime());
%let date_human=%sysfunc(putc(%sysfunc(putn(&now,B8601DN8))%sysfunc(timepart(&now),B8601TM6), $14.));
%let filename = monitoring_report;
%let typ = .xlsx;

%let fname = &filename.&date_human.&typ;
ods excel file=&date_human options(sheet_name = Summary sheet_interval='none' embedded_titles='yes' start_at='2,6');
%mend;
%create_xls();



data my_data2;  
    set my_data1;
    by var1;
    if first.var1 then row_number=0;
    row_number+1;
run;