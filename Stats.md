- Lojistik regresyon analizi sonucunda elde edilen modelin uygun olup olmadığı “model ki-kare” testi ile, her bir bağımsız değişkenin modelde varlığının anlamlı olup olmadığı ise Wald istatistiği ile test edilir. Wald ki-kare istatistiği t değerlerinin karesine eşittir.
- [https://oguzerdo.medium.com/crm-analytics-cltv-projeksiyonu-54a0bfa99b1f]()
- [https://oguzerdo.medium.com/rfm-analizi-ile-m%C3%BC%C5%9Fteri-segmentasyonu-e4ddd00d1de8]()
- [https://oguzerdo.medium.com/crm-analytics-cltv-hesaplama-d75cac75a5dd]()
- [https://en.wikipedia.org/wiki/Generalized_linear_model]()
- [https://en.wikipedia.org/wiki/Bayesian_statistics]()
- R² tek bir ölçüt yöntemi değildir ve çoğu zaman diğer metrik ölçütlere (SSE, MSE, MAE, MAPE, RMSE vb.) de bakılması gerekir.
- [https://www.listendata.com/2019/09/gini-cumulative-accuracy-profile-auc.html]()
- [https://www.listendata.com/2015/01/model-performance-in-logistic-regression.html]()
- [https://www.listendata.com/2019/09/roll-rate-analysis.html]()
- [https://towardsdatascience.com/using-the-gini-coefficient-to-evaluate-the-performance-of-credit-score-models-59fe13ef420]()
- [https://veribilimcisi.com/scikit-learn]()
- [https://veribilimcisi.com/2017/07/14/mse-rmse-mae-mape-metrikleri-nedir]()
- [https://s3.amazonaws.com/assets.datacamp.com/blog_assets/Scikit_Learn_Cheat_Sheet_Python.pdf]()
- [ https://scikit-learn.org/stable/user_guide.html]()
- [Aykırı Değerlerin Tespiti ve Bu Değerler ile Mücadele Yöntemleri | by Muhammet Bektaş | Medium](https://medium.com/@mbektas/ayk%C4%B1r%C4%B1-de%C4%9Ferlerin-tespiti-ve-bu-de%C4%9Ferler-ile-m%C3%BCcadele-y%C3%B6ntemleri-4f0cf76737d1)

https://github.com/osmanemreoz/Armut-ARL
https://yigitsener.medium.com/veri-bilimi-regresyon-analizlerinde-supervised-%C3%B6nemli-kavramlar-rehberi-cheat-sheet-347bd796ec74
http://www.veridefteri.com/2022/02/07/zaman-serileri-analizi-9-guncel-ongoru-yaklasimlari/
https://medium.com/deep-learning-turkiye
https://github.com/deeplearningturkiye/turkce-yapay-zeka-kaynaklari#bloglar
https://www.linkedin.com/feed/update/urn:li:activity:6696018914209476608/?updateEntityUrn=urn%3Ali%3Afs_feedUpdate%3A%28V2%2Curn%3Ali%3Aactivity%3A6696018914209476608%29
https://yigitsener.medium.com/makine-%C3%B6%C4%9Frenmesi-ile-m%C3%BC%C5%9Fteri-kayb%C4%B1-churn-olas%C4%B1l%C4%B1k-tahminlemesi-bankac%C4%B1l%C4%B1k-sekt%C3%B6r%C3%BCnden-%C3%B6rnek-f0f6cd37c00e
https://michael-fuchs-python.netlify.app/2019/10/14/roadmap-for-regression-analysis/
https://www.veribilimiokulu.com/veri-hazirliginin-vazgecilmezi-ozellik-olceklendirme/
https://www.veribilimiokulu.com/python-ile-tek-yonlu-varyans-cozumlemesione-way-anova/

Python stepwise yok mu ?
https://datascience.stackexchange.com/questions/937/does-scikit-learn-have-a-forward-selection-stepwise-regression-algorithm
https://scikit-learn.org/stable/modules/generated/sklearn.feature_selection.SequentialFeatureSelector.html

https://www.veribilimiokulu.com/pratik-bilgiler-pratik-komutlar-python-pandas/
https://www.veribilimiokulu.com/feature-selection/